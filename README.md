# django-radulski-albums

`django-radulski-albums` a Django app for creating photo-albums in any Django application. 
Only basic features are provided, but you can easily add your own functionality by overriding a template, 
or writing your own view. 

**Features include:**

 - uploading multiple photos at a time
 - uploading zip file with photos
 - sorting photos using drag-and-drop
 - title (and optional description) for each album
 
Check these websites that use django-radulski-albums

* [www.saintwilliamparish.org](http://www.saintwilliamparish.org/photos/)
* [www.szkolabrataalberta.org](http://www.szkolabrataalberta.org/zdjecia/)




## Installation

django-radulski-albums requires the following software:

- Django 1.6+
- easy-thumbnails 1.5+

Follow these simple steps for quick installation:

1. Install app: `pip install https://bitbucket.org/rafal_radulski/django-radulski-albums/get/master.zip#egg`
3. Add `radulski_albums` to INSTALLED_APPS in settings.py
4. Run `./manage syncdb`
5. Add to urls.py: `url(r'^photos/', include('radulski_albums.urls')),`



## How to Use

1. Create an album in admin
2. Upload photos to your album
3. View the album at http://example.com/photos/



## Authors

* [Rafal Radulski](http://www.radulski.net/)
