import os
from setuptools import setup, find_packages

README = open(os.path.join(os.path.dirname(__file__), 'README.md')).read()

setup(
    name = 'django-radulski-albums',
    version = '0.2',
    author = 'Rafal Radulski',
    author_email = 'rrafal@gmail.com',
    packages = find_packages(exclude=["example"]),
    include_package_data = True,
    install_requires = ['easy-thumbnails'],
    zip_safe = False,
    description = ('Simple Django app for creating and displaying photo-albums.'),
    license = 'BSD License',
    keywords = 'django photos album gallery',
    long_description = README,
    classifiers = [
        'Development Status :: 3 - Alpha',
        'Framework :: Django',
        'Topic :: Utilities',
        'License :: OSI Approved :: BSD License',
    ],
)
