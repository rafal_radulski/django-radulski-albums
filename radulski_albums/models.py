
import datetime

from django.db import models
from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe


class Album(models.Model):
    class Meta(object):
        ordering = ['-date', 'title']

    
    date = models.DateField('Date')
    title = models.CharField('Title',max_length=255, blank=False, null=False)
    slug = models.SlugField('Slug', unique=True, help_text="It's used for URL. It's automatically generated for you.")
    description = models.TextField('Description', blank=True, null=True)
    is_public = models.BooleanField('Is public', default=True)
    cover_image= models.ImageField('Cover Image', upload_to='albums', null=True, blank=True)
    
    def __unicode__(self):
        return self.title
    
    def get_absolute_url(self):
        return reverse('radulski_album_detail', args=[self.slug])
    
    @property
    def any_cover_image(self):
        ''' Find an image that can be shown as a cover image '''
        if self.cover_image:
            return self.cover_image
        
        for photo in self.photo_set.all():
            if photo.image:
                return photo.image
        return None
    
class Photo(models.Model):
    class Meta(object):
        ordering = ['position', 'id']

    
    image = models.ImageField('Image', upload_to='albums')
    album = models.ForeignKey(Album)
    position = models.PositiveIntegerField(blank=False, null=False, default=10)
    
    def render_thumbnail(self):
        from easy_thumbnails.files import get_thumbnailer
        
        url = get_thumbnailer(self.image)['photo_thumbnail'].url
        return mark_safe('<img src="%s" alt="%s" />' % (url, unicode(self)) ) 
    
    def __unicode__(self):
        return '%s - %s' % (self.album.title, self.id) 
    
