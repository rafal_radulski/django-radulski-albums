
from django.test import TestCase

from .models import Album, Photo



def create_image_file(filename="test.png"):
    from StringIO import StringIO
    from PIL import Image
    from django.core.files.base import ContentFile

    buffer = StringIO()
    image = Image.new("RGBA", size=(50,50), color=(256,0,0))
    image.save(buffer, 'png')
    buffer.seek(0)
    
    return ContentFile(buffer.read(), filename)


def create_zip_file(files, filename="test.zip"):
    from zipfile import ZipFile
    from StringIO import StringIO
    from django.core.files.base import ContentFile
    
    buffer = StringIO()  
    zip = ZipFile(buffer, "a")  
    
    for file in files:
        zip.writestr(file.name, file.read())  
          
    zip.close()
    buffer.seek(0)
    
    return ContentFile(buffer.read(), filename) 
    
class ModelsTest(TestCase):
    def test_any_cover_image__simple(self):
        album = Album(title='Album A')
        album.cover_image = create_image_file()
        album.save()
        
        self.assertIsNotNone( album.any_cover_image )
        
    def test_any_cover_image__from_photo(self):
        album = Album(title='Album A')
        album.save()
        
        photo = Photo(album=album)
        photo.image = create_image_file()
        photo.save()
        
        self.assertIsNotNone( album.any_cover_image )

    def test_any_cover_image__prefer_album(self):
        album = Album(title='Album A')
        album.cover_image = create_image_file()
        album.save()
        
        photo = Photo(album=album)
        photo.image = create_image_file()
        photo.save()
        
        self.assertNotEquals(photo.image, album.any_cover_image)
        self.assertEquals(album.cover_image, album.any_cover_image)

    def test_any_cover_image__none(self):
        album = Album(title='Album A')
        album.save()
        
        photo = Photo(album=album)
        photo.save()
        
        self.assertIsNone(album.any_cover_image)

    
    

class AdminTest(TestCase):
    def setUp(self):
        from django.contrib.auth.models import User
        from django.test.client import Client
        
        user = User.objects.create_superuser('admin', 'admin@example.com', 'secret')
        self.client = Client()
        self.client.login(username="admin", password="secret")


    def test_upload_form(self):
        Album(id=10, title='Album 10').save()
        
        response = self.client.get('/admin/radulski_albums/album/10/upload/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Upload to ')
    
    def test_upload_single_image(self):
        Album(id=10, title='Album 10').save()
        
        # upload
        file_a = create_image_file()
        data = {
            'files' : [file_a]
            }
        response = self.client.post('/admin/radulski_albums/album/10/upload/', data)
        
        # test
        album = Album.objects.get(id=10)
        self.assertEquals(1, album.photo_set.count())
            
    def test_upload_images(self):
        Album(id=10, title='Album 10').save()
        
        # upload
        files = [create_image_file('a.png'), create_image_file('b.png')]
        data = {
            'files' : files
            }
        response = self.client.post('/admin/radulski_albums/album/10/upload/', data)
        
        # test
        album = Album.objects.get(id=10)
        self.assertEquals(2, album.photo_set.count())
    
    def test_upload_zip(self):
        Album(id=10, title='Album 10').save()
        
        # upload
        files = [create_image_file('a.png'), create_image_file('b.png')]
        data = {
            'files' : [create_zip_file(files)]
            }
        response = self.client.post('/admin/radulski_albums/album/10/upload/', data)
        
        # test
        album = Album.objects.get(id=10)
        self.assertEquals(2, album.photo_set.count())
        
    def test_upload_images_and_zips(self):
        Album(id=10, title='Album 10').save()
        
        # upload
        file_a = create_image_file('a.png')
        file_b = create_image_file('b.png')
        file_c = create_zip_file([create_image_file('c1.png'),create_image_file('c2.png')], 'c.zip')
        file_d = create_zip_file([create_image_file('d1.png'),create_image_file('d2.png')], 'd.zip')
        
        data = {
            'files' : [file_a, file_b, file_c, file_d]
            }
        response = self.client.post('/admin/radulski_albums/album/10/upload/', data)
        
        # test
        album = Album.objects.get(id=10)
        self.assertEquals(6, album.photo_set.count())
        
