
import os


from django import forms
from django.core.files.uploadedfile import SimpleUploadedFile
from django.utils.translation import ugettext_lazy as _

from .models import Album, Photo

class AlbumUploadForm(forms.Form):
    files = forms.FileField(widget=forms.FileInput(attrs={'multiple':'multiple', 'required':'required'}))
    
    def get_images(self):
        for file in self.files.getlist('files'):
            if file.content_type == 'application/zip':
                 for image in self._process_zip_archive(file):
                     yield image
            else:
                image = self._process_single_file(file)
                if image:
                    yield image

    def _process_single_file(self, file):
        try:
            return forms.ImageField().clean(file)
        except forms.ValidationError:
            return None

    def _process_zip_archive(self, zip_archive):
        import zipfile

        zfile = zipfile.ZipFile(zip_archive)

        for filename in zfile.namelist():
            # get the file and validate it
            try:
                content = zfile.read(filename)
                image_file = SimpleUploadedFile(filename, content)
                yield forms.ImageField().clean(image_file) 
            except forms.ValidationError:
                continue # ignore non-image fieles in zip
