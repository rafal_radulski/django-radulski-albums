from django.conf.urls import patterns, url

from .views import AlbumListView, AlbumDetailView

urlpatterns = patterns('',
    url(r'^$', AlbumListView.as_view(), name="radulski_album_list"),
    url(r'^(?P<slug>[\-\w]+)$', AlbumDetailView.as_view(), name="radulski_album_detail"),
)

