from django.conf.urls import patterns, url
from django.contrib import admin
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.template.response import TemplateResponse
from django.utils.translation import ugettext_lazy as _


from .models import Album, Photo


class PhotoAdmin(admin.ModelAdmin):
    class Media:
        css = {
            "all": ("albums/admin.css",)
        }
        
    model = Photo
    
    fields = ('render_thumbnail', 'image')
    readonly_fields = ('render_thumbnail',)


class AlbumAdmin(admin.ModelAdmin):
    class Media:
        css = {
            "all": ("albums/admin.css",)
        }
    
    change_form_template = 'radulski_albums/admin/album/change_form.html'
    list_display = ('title', 'date', 'is_public')
    prepopulated_fields = {'slug': ('title','date')}

    
    def get_urls(self):
        info = self.model._meta.app_label, self.model._meta.module_name
        default_patterns = super(AlbumAdmin, self).get_urls()
        custom_patterns = patterns('', url(
            r'^(?P<id>[\d]+)/upload/$',
            self.admin_site.admin_view(self.upload_view),
            name='%s_%s_upload' % info
        ))
        return custom_patterns + default_patterns

    def save_model(self, request, obj, form, change):
        if request.POST.get('photos'):
            # sort photos 
            for photo in obj.photo_set.all():
                key = 'photos_%d' % (photo.id,)
                if key in request.POST:
                    photo.position = request.POST.get(key)
                    photo.save()
                else:
                    photo.delete()
        obj.save()

    def upload_view(self, request, id, template='radulski_albums/admin/album/upload.html'):
        from django.core.exceptions import PermissionDenied
        from django.shortcuts import get_object_or_404
        from .forms import AlbumUploadForm
        

        album = get_object_or_404(Album, pk=id)
        opts = Album._meta
        
        if not self.has_change_permission(request, album):
            raise PermissionDenied
        
        if request.method == 'POST':
            form = AlbumUploadForm(request.POST, request.FILES)
            if form.is_valid():
                for image in form.get_images():
                    photo = Photo(album=album, image=image)
                    photo.save()

                # Send user to album after upload
                url = reverse('admin:radulski_albums_album_change', args=[album.pk])
                return HttpResponseRedirect(url + '#photos-group')
        else:
            form = AlbumUploadForm()

        context = {
            'app_label': opts.app_label,
            'has_add_permission': self.has_add_permission(request),
            'has_change_permission': self.has_change_permission(request),
            'module_name': opts.verbose_name_plural,
            'opts': opts,
            'title': _('Upload to "%s"') % (album.title,),
            'form': form,
        }
        return TemplateResponse(request, template, context, current_app=self.admin_site.name)

    


admin.site.register(Album, AlbumAdmin)
admin.site.register(Photo, PhotoAdmin)
