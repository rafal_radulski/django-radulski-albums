
from django.views.generic import ListView, DetailView

from .models import Album

class AlbumListView(ListView):
    queryset = Album.objects.filter(is_public=True)
    
    template_name = 'radulski_albums/album_list.html'
    allow_empty = True

class AlbumDetailView(DetailView):
    queryset = Album.objects.filter(is_public=True)
    
    template_name = 'radulski_albums/album_detail.html'
    allow_empty = True
    
    
